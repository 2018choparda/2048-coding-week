import random
import copy

def choose_random_move(grid):
    l=move_possible(grid)
    move=[]
    for i in range(len(l)):
        if l[i]:
            if i==0:
                move.append("left")
            elif i==1:
                move.append("right")
            elif i==2:
                move.append("up")
            elif i==3:
                move.append("down")
    n=random.randint(0,len(move)-1)
    return move[n]


    
    
    

def random_play(n,theme):
    grid=conv(init_game(n))  #conv convertit les vides de init game en 0
    
    while not is_game_over(grid):
        move=choose_random_move(grid)
        move_grid(grid, move)
        pos=get_new_position(grid)
        grid=grid_add_new_tile_at_position(grid, pos[0], pos[1])
        print(grid_to_string_with_size_and_theme(grid,2,theme))
    if get_grid_tile_max(grid)==2048:
        return("victoire")
    else:
        return("game over")
        
def ask_and_read_grid_size():
    size= int(input("Entrez la taille de la grille ( entier >0 ) : "))
    return size
        
def ask_and_read_grid_theme():
    theme = input( "Sélectionnez un thème : 0 pour le classique, 1 pour thème chimie, 2 pour thème alphabet :")
    return theme
def ask_and_read_command():
    command=input("direction right or left or up or down ")
    return command

def game_play():
    size=ask_and_read_grid_size()
    theme=ask_and_read_grid_theme()
    
    grille=conv(init_game(size))
    print(grid_to_string_with_size_and_theme(grille,2,theme))
    while not is_game_over(grille):
        move=ask_and_read_command()
        
        move_grid(grille, move)
        pos=get_new_position(grille)
        grille=grid_add_new_tile_at_position(grille, pos[0], pos[1])
        print(grid_to_string_with_size_and_theme(grille,2,theme))
    if get_grid_tile_max(grille)==2048:
        return("victoire")
    else:
        return("game over")
        
        
    
        



































def create_grid(n):
 s = []
 for i in range(n):
   s.append([' ']*n)
 return s

def test_is_full_grid():
  assert is_full_grid(create_grid(4))==False
  grille_pleine=[]
  for i in range(4):
     grille_pleine.append([4]*4)
  assert is_full_grid(grille_pleine)==True


def move_possible(grid):
    B=copy.deepcopy(grid)
    
    return([not is_full_grid(move_grid(B,"left")),not is_full_grid(move_grid(B,"right")),not is_full_grid(move_grid(B,"up")),not       is_full_grid(move_grid(B,"down"))])
    
def test_move_possible():
    assert move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == [True,True,False,True]      
    assert move_possible([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == [False,False,False,False]

def is_game_over(grid):
    if move_possible(grid)==[False,False,False,False]:
        print('game over')
        return(True)
    else:
        return(False)

def get_grid_tile_max(grid):
    max=0
    for i in range(len(grid)):
        for j in range(len(grid)):
            if grid[i][j]>max:
                max=grid[i][j]
    return(max)
    
def move_grid(grid,d):
    n=len(grid)
    if d=="left":
        for i in range(n):
            grid[i]=move_row_left(grid[i])
    if d=="right":
        for i in range(n):
            grid[i]=move_row_right(grid[i])
    if d=="up":
        for i in range(n):
            L=[]
            for j in range(n):
                L.append(grid[j][i])
            new=move_row_left(L)
            for j in range(n):
                grid[j][i]=new[j]
    if d=="down":
        for i in range(n):
            L=[]
            for j in range(n):
                L.append(grid[j][i])
            new=move_row_right(L)
            for j in range(n):
                grid[j][i]=new[j]
    return grid
            
def is_full_grid(grid):
  k=0
  for i in range(len(grid)):
   for j in range(len(grid)):
     if grid[i][j]==0 or grid[i][j]==' ':
        k+=1
   return(k==0)

        





def move_row_left(L):
    k=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k+=1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    for i in range(len(L)-1):
        if L[i]==L[i+1]:
                    L[i]=2*L[i+1]
                    L[i+1]=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k+=1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    return(L)

def move_row_right(L):
    G=[0 for i in range(len(L))]
    for i in range(len(L)):
        G[i]=L[len(L)-1-i]
        H=move_row_left(G)
        J=[0 for i in range(len(L))]
        for i in range(len(L)):
            J[i]=H[len(L)-1-i]
    return(J)



def test_create_grid():
    assert create_grid() == [[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' ']]

def create_grid(n):
    game_grid = []
    for i in range(0,n):
        game_grid.append([' ']*n)
    return game_grid

def test_grid_add_new_tile_at_position():
    game_grid=create_grid(4)
    game_grid=grid_add_new_tile_at_position(game_grid,1,1)
    tiles = get_all_tiles(game_grid)
    assert 2 or 4 in tiles


def get_value_new_tile():
    if random.random()<0.9:
        n=2
    else:
        n=4
    return n

def test_get_value_new_tile():
    assert get_value_new_tile()== 2 or 4



def grid_add_new_tile_at_position(grille,x,y):

    grille[x][y]=get_value_new_tile()
    return grille

def get_all_tiles(grille):
    alltiles=[]
    for i in range (len(grille)):
        for j in range (len(grille[i])):
            if grille[i][j]==' ':
                alltiles.append(0)
            else:
                alltiles.append(grille[i][j])
    return alltiles

def test_get_all_tiles():
    assert get_all_tiles( [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]) == [0,4,8,2,0,0,0,0,0,512,32,64, 1024,2048,512,0]
    assert get_all_tiles([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]]) == [16, 4, 8, 2, 2, 4, 2, 128, 4, 512, 32, 64, 1024, 2048, 512, 2]
    assert get_all_tiles(create_grid(3))== [0 for i in range(9)]


def get_empty_tiles_positions(grille):
    emptytiles=[]
    for i in range(len(grille)):
        for j in range(len(grille[i])):
            if grille[i][j]==0 or grille[i][j]==' ':
                emptytiles.append((i,j))
    return emptytiles

def test_get_empty_tiles_positions():
    assert get_empty_tiles_positions([[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions([[' ', 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions(create_grid(2))==[(0,0),(0,1),(1,0),(1,1)]
    assert get_empty_tiles_positions([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]])==[]

def get_new_position(grille):
    positions=get_empty_tiles_positions(grille)
    n=random.randint(0,len(positions)-1)
    nouvellepos=positions[n]
    return nouvellepos

def grid_get_value(grille,x,y):
    if grille[x][y]==' ':
        return 0
    else:
        return grille[x][y]



def test_get_new_position():
    grid = [[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0
    grid = [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0

def init_game(n):
    grille=grid_add_new_tile_at_position(create_grid(n),get_new_position(create_grid(n))[0],get_new_position(create_grid(n))[1])
    
    return grid_add_new_tile_at_position(grille,get_new_position(grille)[0],get_new_position(grille)[1])
    
def conv(grille):
    for i in range(len(grille)):
        for j in range(len(grille)):
            if grille[i][j]==' ':
                grille[i][j]=0
    return grille
    
    
def test_init_game():
    grid = init_game(4)
    tiles = get_all_tiles(grid)
    assert 2 or 4 in tiles
    assert len(get_empty_tiles_positions(grid)) == 14

THEMES = {
        "0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256",
              512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
        "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O",
              512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
        "2": {"name": "Alphabet", 0: " ", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H",
              512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

def grid_to_string_with_size_and_theme(grid,n,theme):
    THEMES = {
        "0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256",
              512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
        "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O",
              512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
        "2": {"name": "Alphabet", 0: " ", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H",
              512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
   
    
    size = len(grid)
    string = """"""
    entre_lignes=" "+("="*n+" ")*size + "\n"
    string+=entre_lignes
    for i in range(size):
        string += "|"
        for j in range(size):
            ch=THEMES[theme][grid[i][j]]
            while len(ch)<n:
                ch+=" "
            string+=ch
            string+="|"
        string+="\n"
        string+=entre_lignes
    print(string)
    return(string)
    
def test_grid_to_string_with_size_and_theme():
    grid=[[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 64], [1024, 2048, 512, 2]]
    a="""
=============
|Be|He|Li|H |
=============
|H |He|H |N |
=============
|He|F |B |C |
=============
|Ne|Na|F |H |
=============
"""
    assert grid_to_string_with_size_and_theme(grid,2,"1")== a[1:]


def long_value_grid(grid):
    
    n=len(str(grid[0][0]))
    for i in range(len(grid)):
        for j in range(len(grid)):
            if len(str(grid[i][j]))>n:
                n=len(str(grid[i][j]))
    return n
        
    


def create_grid(n):
    game_grid = []
    for i in range(0,n):
        game_grid.append([' ']*n)
    return game_grid
    

        
    
    