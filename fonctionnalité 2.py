THEMES = {
        "0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256",
              512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
        "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O",
              512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
        "2": {"name": "Alphabet", 0: " ", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H",
              512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

def grid_to_string_with_size_and_theme(grid,n,theme):
    THEMES = {
        "0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256",
              512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
        "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O",
              512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
        "2": {"name": "Alphabet", 0: " ", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H",
              512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
   
    
    size = len(grid)
    string = """"""
    entre_lignes=" "+("="*n+" ")*size + "\n"
    string+=entre_lignes
    for i in range(size):
        string += "|"
        for j in range(size):
            ch=THEMES[theme][grid[i][j]]
            while len(ch)<n:
                ch+=" "
            string+=ch
            string+="|"
        string+="\n"
        string+=entre_lignes
    print(string)
    return(string)
    
def test_grid_to_string_with_size_and_theme():
    grid=[[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 64], [1024, 2048, 512, 2]]
    a="""
=============
|Be|He|Li|H |
=============
|H |He|H |N |
=============
|He|F |B |C |
=============
|Ne|Na|F |H |
=============
"""
    assert grid_to_string_with_size_and_theme(grid,2,"1")== a[1:]


def long_value_grid(grid):
    
    n=len(str(grid[0][0]))
    for i in range(len(grid)):
        for j in range(len(grid)):
            if len(str(grid[i][j]))>n:
                n=len(str(grid[i][j]))
    return n
        
    


def create_grid(n):
    game_grid = []
    for i in range(0,n):
        game_grid.append([' ']*n)
    return game_grid
    


