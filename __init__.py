import random


def test_create_grid():
    assert create_grid() == [[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' ']]

def create_grid(n):
    game_grid = []
    for i in range(0,n):
        game_grid.append([' ']*n)
    return game_grid

def test_grid_add_new_tile_at_position():
    game_grid=create_grid(4)
    game_grid=grid_add_new_tile_at_position(game_grid,1,1)
    tiles = get_all_tiles(game_grid)
    assert 2 or 4 in tiles


def get_value_new_tile():
    if random()<0.9:
        n=2
    else:
        n=4
    return n

def test_get_value_new_tile():
    assert get_value_new_tile()== 2 or 4



def grid_add_new_tile_at_position(grille,x,y):

    grille[x][y]=nbralea
    return grille

def get_all_tiles(grille):
    alltiles=[]
    for i in range (len(grille)):
        for j in range (len(grille[i])):
            if grille[i][j]==' ':
                alltiles.append(0)
            else:
                alltiles.append(grille[i][j])
    return alltiles

def test_get_all_tiles():
    assert get_all_tiles( [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]) == [0,4,8,2,0,0,0,0,0,512,32,64, 1024,2048,512,0]
    assert get_all_tiles([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]]) == [16, 4, 8, 2, 2, 4, 2, 128, 4, 512, 32, 64, 1024, 2048, 512, 2]
    assert get_all_tiles(create_grid(3))== [0 for i in range(9)]


def get_empty_tiles_positions(grille):
    emptytiles=[]
    for i in range(len(grille)):
        for j in range(len(grille[i])):
            if grille[i][j]==0 or grille[i][j]==' ':
                emptytiles.append((i,j))
    return emptytiles

def test_get_empty_tiles_positions():
    assert get_empty_tiles_positions([[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions([[' ', 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions(create_grid(2))==[(0,0),(0,1),(1,0),(1,1)]
    assert get_empty_tiles_positions([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]])==[]

def get_new_position(grille):
    positions=get_empty_tiles_positions(grille)
    n=random.randint(0,len(positions)-1)
    nouvellepos=positions[n]
    return nouvellepos

def grid_get_value(grille,x,y):
    if grille[x][y]==' ':
        return 0
    else:
        return grille[x][y]



def test_get_new_position():
    grid = [[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0
    grid = [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0

def init_game(n):
    grille=create_grid(n)
    a=
