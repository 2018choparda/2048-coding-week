#fonctionnalité 4
#pdt = position de l'élément en cours de traitement

def move_grid(grid, direction):
    if direction=="right":
        for i in range (len(grid)):
            grid[i]=move_row_right(grid[i])
    elif direction=="left":
        for i in range (len(grid)):
            grid[i]=move_row_left(grid[i])
    elif direction=="down":
        for i in range (len(grid)):
            for j in range(i):
                grid[i][j],grid[j][i]=grid[j][i],grid[i][j]
        for i in range (len(grid)):
            grid[i]=move_row_right(grid[i])
        for i in range(len(grid)):
            for j in range(i):
                grid[i][j],grid[j][i]=grid[j][i],grid[i][j]
    elif direction=="up":
        for i in range(len(grid)):
            for j in range(i):
                grid[i][j],grid[j][i]=grid[j][i],grid[i][j]
        for i in range (len(grid)):
            grid[i]=move_row_left(grid[i])
        for i in range(len(grid)):
            for j in range(i):
                grid[i][j],grid[j][i]=grid[j][i],grid[i][j]
    return grid
        





def move_row_left(L):
    k=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k+=1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    for i in range(len(L)-1):
        if L[i]==L[i+1]:
                    L[i]=2*L[i+1]
                    L[i+1]=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k+=1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    return(L)

def move_row_right(L):
    G=[0 for i in range(len(L))]
    for i in range(len(L)):
        G[i]=L[len(L)-1-i]
        H=move_row_left(G)
        J=[0 for i in range(len(L))]
        for i in range(len(L)):
            J[i]=H[len(L)-1-i]
    return(J)
    
def test_move_grid():
    assert move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"left") == [[4,0,0,0], [8, 0, 0, 0], [16, 0, 0, 0], [4, 0, 0, 0]]
    assert move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"right") == [[0,0,0,4], [0, 0, 0, 8], [0, 0, 0, 16], [0, 0, 0, 4]]
    assert move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"up") == [[4,8,4,2], [16, 2, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    assert move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"down") == [[0, 0, 0, 0], [0, 0, 0, 0],[4,8,0,0],[16, 2, 4, 2]]









    
    
    
        
        